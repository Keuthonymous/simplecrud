﻿import React, { Component } from 'react';
import ImageUploader from 'react-images-upload';
import Alert from 'react-bootstrap/Alert';

export class EditCard extends Component {
    static displayName = EditCard.name;

    constructor(props) {
        super(props);
        this.state = {
            showSuccess: false,
            currCardId: props.match.params.id,
            visitkort: {
                FirstName: '',
                LastName: '',
                Email: '',
                PhoneNumber: '',
                Image: ''
            }
        };
        this.onDrop = this.onDrop.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.hideAlert = this.hideAlert.bind(this);
        this.getCardById();
    }

    onDrop(picture) {
        this.getBase64(picture[0], (result) => {
            this.state.visitkort.image = result
            this.setState(() => { return this.state.visitkort });
        });
    }

    onChange(event) {
        const target = event.target;
        this.setState(prevState => ({
            visitkort: {
                ...prevState.visitkort,
                [target.name]: target.value
            }
        }));
    }

    onSubmit(event) {
        const state = this.state;
        event.preventDefault();

        const visitkort = state.visitkort;
        fetch("Visitkorts/" + this.state.currCardId, {
            method: "PUT",
            body: JSON.stringify(visitkort),
            headers: { 'Content-Type': 'application/json' }
        }).then((response) => {
            if (response.ok) {
                this.setState({ showSuccess: true });
            }
        });

    }

    getCardById() {
        const id = this.state.currCardId;
        fetch("Visitkorts/" + id)
            .then(response => (response.json()))
            .then(data => (this.setState(() => { return { visitkort: data } })));
    }

    getBase64(file, callback) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function () {
            callback(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        }
    }

    hideAlert() {
        this.setState({ showSuccess: false });
    }

    render() {
        return (
            <div>
                <Alert
                    show={this.state.showSuccess}
                    variant="success"
                    onClose={this.hideAlert}
                    dismissible
                >
                    <p>Changes have been saved!</p>
                </Alert>
                <h1>Redigera Visitkort</h1>
                <form onSubmit={this.onSubmit} >
                    <div class="form-group">
                        <label for="firstName">Namn</label>
                        <input type="text" class="form-control" value={this.state.visitkort.firstName} name="firstName" id="firstName" aria-describedby="firstName" placeholder="Namn" required onChange={this.onChange} />
                    </div>
                    <div class="form-group">
                        <label for="lastName">Efternamn</label>
                        <input type="text" class="form-control" value={this.state.visitkort.lastName} name="lastName" id="lastName" aria-describedby="lastName" placeholder="Efter Namn" required onChange={this.onChange} />
                    </div>
                    <div class="form-group">
                        <label for="phoneNumber">Telefon</label>
                        <input type="tel" class="form-control" value={this.state.visitkort.phoneNumber} name="phoneNumber" id="phoneNumber" aria-describedby="phoneNumber" placeholder="Telefon" required onChange={this.onChange} />
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" value={this.state.visitkort.email} name="email" id="email" aria-describedby="email" placeholder="Namn" required onChange={this.onChange} />
                    </div>
                    <div class="container" style={{ maxWidth: 10 + 'rem', maxHeight: 10 + 'rem' }}>
                        <img src={this.state.visitkort.image} style={{ maxWidth: 100 + '%' }} />
                    </div>
                    <div class="form-group">
                        <ImageUploader
                            withIcon={true}
                            buttonText='Välj bild'
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.gif']}
                            maxFileSize={5242880}
                        />
                    </div>
                    <button type="submit" class="btn btn-primary mr-1">Spara</button>
                    <a href="/" class="btn btn-secondary">Avbryta</a>
                </form>
            </div>
        );
    }
}