﻿import React, { Component } from 'react';
import ImageUploader from 'react-images-upload';
import Alert from 'react-bootstrap/Alert';

export class Create extends Component {
    static displayName = Create.name;

    constructor(props) {
        super(props);

        this.state = {
            showSuccess: false,
            showError: false,
            visitkort: {
                FirstName: '',
                LastName: '',
                Email: '',
                PhoneNumber: '',
                Image: ''
            }
        };
        this.onDrop = this.onDrop.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.hideAlert = this.hideAlert.bind(this);
        this.hideError = this.hideError.bind(this);
    }

    onDrop(picture) {
        this.getBase64(picture[0], (result) => {
            this.state.visitkort.Image = result;
            this.setState(this.state.visitkort);
        });
        this.hideError();
    }

    onChange(event) {
        const target = event.target;
        this.state.visitkort[target.name] = target.value;
        this.setState(this.state.visitkort);
    }

    onSubmit(event) {
        const state = this.state;
        const visitkort = state.visitkort;
        event.preventDefault();
        if (visitkort.Image == '') {
            this.setState({ showError: true });
            return;
        } else {
            fetch("Visitkorts", {
                method: "POST",
                body: JSON.stringify(visitkort),
                headers: { 'Content-Type': 'application/json' }
            }).then((response) => {
                if (response.ok) {
                    this.setState({ showSuccess: true });
                }
            });
        }
    }

    getBase64(file, callback) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function () {
            callback(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        }
    }

    hideAlert() {
        this.setState({ showSuccess: false });
    }

    hideError() {
        this.setState({ showError: false });
    }

    render() {
        return (
            <div>
                <Alert
                    show={this.state.showSuccess}
                    variant="success"
                    onClose={this.hideAlert}
                    dismissible
                >
                    <p>Kortet har skapats</p>
                </Alert>
                <Alert
                    show={this.state.showError}
                    variant="danger"
                    onClose={this.hideError}
                    dismissible
                >
                    <p>Du måste lägga upp en bild</p>
                </Alert>
                <h1>Skapa Visitkort</h1>
                <form onSubmit={this.onSubmit} onChange={this.onChange}>
                    <div class="form-group">
                        <label for="firstName">Namn</label>
                        <input type="text" class="form-control" name="FirstName" id="firstName" aria-describedby="firstName" placeholder="Namn" required/>
                    </div>
                    <div class="form-group">
                        <label for="lastName">Efter Namn</label>
                        <input type="text" class="form-control" name="LastName" id="lastName" aria-describedby="lastName" placeholder="Efter Namn" required/>
                    </div>
                    <div class="form-group">
                        <label for="phoneNumber">Telefon</label>
                        <input type="tel" class="form-control" name="PhoneNumber" id="phoneNumber" aria-describedby="phoneNumber" placeholder="Telefon" required/>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="Email" id="email" aria-describedby="email" placeholder="Namn" required/>
                    </div>
                    <div class="form-group">
                        <ImageUploader
                            withIcon={true}
                            buttonText='Välja bild'
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.gif']}
                            maxFileSize={5242880}
                        />
                    </div>
                    <button type="submit" class="btn btn-primary mr-1">Skapa</button>
                    <a href="/" class="btn btn-secondary">Avbryta</a>
                </form>
            </div>
        );
    }
}