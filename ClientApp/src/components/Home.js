import React, { Component } from 'react';
import Alert from 'react-bootstrap/Alert';
import '../custom.css'

export class Home extends Component {
    static displayName = Home.name;
    constructor(props) {
        super(props);
        this.state = {
            cards: [],
            showDeleteAlert: false
        };
        this.getCards();
        this.deleteCard = this.deleteCard.bind(this);
        this.hideDeleteAlert = this.hideDeleteAlert.bind(this)
    }

    getCards() {
        fetch("Visitkorts", {
            method: "GET"
        }).then((data) => data.json())
            .then((response) => {
                this.setState(() => { return { cards: response } });
            });
    }

    deleteCard(id) {
        fetch("Visitkorts/" + id, {
            method: "DELETE"
        }).then((response) => {
            if (response.ok) {
                this.getCards();
                this.setState({ showDeleteAlert: true });
            }
        });
    }

    hideDeleteAlert() {
        this.setState({ showDeleteAlert: false });
    }

    render() {
        const cardItems = this.state.cards.map((card, i) => (
            <div key={card.id} class="col-lg-4 d-flex align-items-stretch">
                <div class="card ml-1 mb-3" style={{ width: 18 + 'rem' }}>
                    <img src={card.image} class="card-img-top" style={{ maxWidth: 100 + '%', maxHeight: 100 + '%' }} />
                    <div class="card-body">
                        <h5 class="card-title">{card.firstName + ' ' + card.lastName}</h5>
                        <p class="card-text">Email: {card.email} <br/>Telefon: {card.phoneNumber}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a href={'/editcard/' + card.id} class="btn btn-sm btn-outline-secondary">Redigera</a>
                                <button type="button" class="btn btn-sm btn-outline-secondary" onClick={() => { this.deleteCard(card.id) }}>Ta bort</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ));

        return (
            <div>
                <Alert
                    show={this.state.showDeleteAlert}
                    variant="success"
                    onClose={this.hideDeleteAlert}
                    dismissible
                >
                    <p>Kortet har tagits bort</p>
                </Alert>
                <div class="row">
                    <h1>Visitkort</h1>
                </div>
                <div class="container">
                    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                        {cardItems}
                    </div>
                </div>
            </div>
        );
    }
}
