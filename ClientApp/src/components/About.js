﻿import React, { Component } from 'react';

export class About extends Component {
    static displayName = About.name;

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <h1>Addressing some things</h1>
                <p> React is something I am not the greatest at, though I did give it a good shot. I am aware that there is room for refactoring, but time constraints made that less of a priority.
                    I also wanted to do things like move the <i>Create</i> and <i>Edit</i> components into modals that would pop up in the <i>Home</i> component, but this idea had to be abandoned again due to
                    time constraints. Transitions were another thing that ended up on the proverbial cutting room floor, but again, they felt less important than delivering an application that 
                    actually functions.
                </p>
                <br />
                <p>
                    This application is built using <a href="https://docs.microsoft.com/en-us/dotnet/fundamentals/" target="_blank">.Net Core 5.0</a> as the backend with React as the frontend. The backend portion is what I found easier, as there really wasn't much to do there. This, for me at least,
                    was more of an exercise in frontend design and React overall. I chose to build it this way because I come from a C# background, and assumed that would be the easiest way for me to do it. That
                    being said, I have never built <i>this exact type</i> of application before with .Net Core and React, and it was a bit of a challenge. It was fun, almost refreshing actually, to get to quickly
                    whip something up from scratch.<br />

                    Some of the packages used:
                    <ul>
                        <li><a href="https://getbootstrap.com/" target="_blank">Bootstrap</a> (it was the default, and what I have most experince with. It was used for alerts, layout, and general formatting)</li>
                        <li><a href="https://www.npmjs.com/package/react-images-upload" target="_blank">react-images-upload</a> (for uploading images)</li>
                        <li><a href="https://docs.microsoft.com/en-us/ef/core/" target="_blank">Entity Framework Core</a> (for database creation and model handling)</li>
                    </ul>
                    <br />
                    <i> Side note: I am aware that pictures shouldn't really be stored in a database as a base64, but that's what the instructions said to do, so that's what I have done. This makes file
                    sizes about 33% bigger over raw binary format. However, saving URLs as base64 in a database is not
                    a bad way of doing things. The point is that the instructions called for a base64 image, so that is what I implemented.
                        </i>
                </p>
                <br />
                <p>
                    I hope I get a chance to talk to someone in further detail about my work here, and that I can address any further questions that might exist. I am aware that this is maybe not the <i>cleanest </i>
                     code to have ever been written, but 24 hours doesn't feel like as much time as it sounds.
                </p>
                <br />
                <p>
                    Thank you for giving me the opportunity to do this task, and I look forward to hearing back from you at CGI.<br />
                    <strong>-Mike</strong>
                </p>
            </div>
        )
    }
}