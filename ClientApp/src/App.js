import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Create } from './components/Create';
import { About } from './components/About';
import { EditCard } from './components/EditCard';

import './custom.css'

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Home} />
                <Route path='/create' component={Create} />
                <Route path='/about' component={About} />
                <Route path='/editcard/:id/' component={EditCard} />
            </Layout>
        );
    }
}
