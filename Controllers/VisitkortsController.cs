﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleCRUD.Data;
using SimpleCRUD.Models;

namespace SimpleCRUD.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class VisitkortsController : ControllerBase
    {
        private readonly SimpleCRUDContext _context;

        public VisitkortsController(SimpleCRUDContext context)
        {
            _context = context;
        }

        // GET: api/Visitkorts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Visitkort>>> Get()
        {
            return await _context.Visitkorts.ToListAsync();
        }

        // GET: api/Visitkorts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Visitkort>> GetById(int id)
        {
            var visitkort = await _context.Visitkorts.FindAsync(id);

            if (visitkort == null)
            {
                return NotFound();
            }

            return visitkort;
        }

        // PUT: api/Visitkorts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Visitkort visitkort)
        {
            if (id != visitkort.ID)
            {
                return BadRequest();
            }

            _context.Entry(visitkort).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VisitkortExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Visitkorts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Visitkort>> PostVisitkort([FromBody] Visitkort visitkort)
        {
            _context.Visitkorts.Add(visitkort);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVisitkort", new { id = visitkort.ID }, visitkort);
        }

        // DELETE: api/Visitkorts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVisitkort(int id)
        {
            var visitkort = await _context.Visitkorts.FindAsync(id);
            if (visitkort == null)
            {
                return NotFound();
            }

            _context.Visitkorts.Remove(visitkort);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool VisitkortExists(int id)
        {
            return _context.Visitkorts.Any(e => e.ID == id);
        }
    }
}
