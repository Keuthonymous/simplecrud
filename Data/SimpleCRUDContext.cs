﻿using Microsoft.EntityFrameworkCore;
using SimpleCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleCRUD.Data
{
    public class SimpleCRUDContext : DbContext
    {
        public SimpleCRUDContext(DbContextOptions<SimpleCRUDContext> options)
            : base(options)
        { 
        }

        public DbSet<Visitkort> Visitkorts { get; set; }
    }
}
